<?php


namespace bd\models;

class Commentaire extends \Illuminate\Database\Eloquent\Model{
	
	  static function ajouter_Commentaire($titre,$contenu,$dateCreation){
      $c1= new Commentaire();
      $c1->titre = $titre;
      $c1->contenu =$contenu ;
      $c1->dateCreation=$dateCreation;

      $maxJeu = rand(1,Game::max("id"));

      $maxUtilisateur = rand(1,Utilisateur::max("id"));
      $c1->id_utilisateur=$maxUtilisateur;
      $c1->id_game=$maxJeu;
      $c1->save();
  }

  public static function commentairesJeu($idJeu)
  {
    return Commentaire::where('id_game','=',$idJeu)->get();
  }

}