<?php


namespace bd\models;

class Utilisateur extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'utilisateur';
	protected $primaryKey='id';
	public $timestamps = false;
	
	static function ajouter_utilisateur($nom,$prenom,$email,$adresse,$numtel,$datenaiss){
      $user=new Utilisateur();
      $user->nom=$nom;
      $user->prenom=$prenom;
      $user->email=$email;
      $user->adresse=$adresse;
      $user->numtel=$numtel;
      $user->datenaiss=$datenaiss;
      $user->save();
  }


  public function commentaires() {
    return $this->hasMany('\bd\Model\Commentaire','id_utilisateur');
  }
}