<?php


namespace bd\models;

class Character extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'character';
	protected $primaryKey='id';
	public $timestamps = false;
	
	

	public function Game()
	{
        return $this->belongsToMany('bd\models\Genre', 'first_appeared_in_game');
    }
}