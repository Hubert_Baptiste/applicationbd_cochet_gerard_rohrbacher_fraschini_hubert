<?php

namespace bd\models;

class Enemies extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'enemies';
	protected $primaryKey='char1_id';
	public $timestamps = false;

	public function theme(){
    return $this->belongsTo('\Application\models\Theme','liste_id');
  }
}
