<?php

namespace bd\models;

class Platform extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'platform';
	protected $primaryKey='id';
	public $timestamps = false;
	
	public function Game()
    {
        return $this->hasMany('bd\models\Game');
    }
	
	
	public function Company()
    {
        return $this->belongsTo('bd\models\Company');
    }
}