<?php

namespace bd\models;

class Rating_board extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'rating_board';
	protected $primaryKey='id';
	public $timestamps = false;
	
	public function Game_rating()
    {
        return $this->hasMany('bd\models\Game_rating');
    }
}