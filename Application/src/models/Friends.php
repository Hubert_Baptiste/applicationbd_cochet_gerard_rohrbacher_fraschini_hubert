<?php

namespace bd\models;

class Friends extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'friends';
	protected $primaryKey='char1_id';
	public $timestamps = false;

	public function theme(){
    return $this->belongsTo('\Application\models\Theme','liste_id');
  }
}
