<?php

namespace bd\models;

class Company extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'company';
	protected $primaryKey='id';
	public $timestamps = false;

	 public function Platform()
    {
        return $this->hasMany('bd\models\Platform');
    }
	
	public function Game()
    {
        return $this->belongsToMany('bd\models\Game', 'publishers');
    }
}
