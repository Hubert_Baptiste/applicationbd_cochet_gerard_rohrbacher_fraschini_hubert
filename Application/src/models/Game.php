<?php

namespace bd\models;

class Game extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'game';
	protected $primaryKey='id';
	public $timestamps = false;

	public function Theme(){	
		return $this->belongsToMany('bd\models\Theme','game_theme');
	}
	
	public function Company()
    {
        return $this->belongsToMany('bd\models\Company', 'developers');
    }
	
	public function Platform()
    {
        return $this->belongsToMany('bd\models\Platform', 'game_platform');
    }
	
	public function Genre()
	{
        return $this->belongsToMany('bd\models\Genre', 'game_genre');
    }
	
	public function Character()
	{
        return $this->belongsToMany('bd\models\Genre', 'appears_in');
    }

	public function Ratings() {
		return $this->belongsToMany('bd\models\Game_Rating','game2rating','game_id','rating_id');
	}

	public function getGame($idjeu){
		  return Game::where('id','=',$idjeu)->first();
	}
	
	public static function listerLes200PremiersJeux($debut){
     return Game::select('*')->skip($debut)->take(200)->get();
   }

	
}
