<?php

namespace bd\Controleur;
use bd\Vues\VueGeneraleSeance1;
use bd\Vues\VueGeneraleSeance2;
use bd\Vues\VueGeneraleSeance5;
class Controleur{

	public function afficherVueGeneraleSeance1(){
		$v = new VueGeneraleSeance1();
		$v->affiche();
	}
	
	
	public function afficherVueGeneraleSeance2(){
		$v = new VueGeneraleSeance2();
		$v->affiche();
	}

	public function afficherVueGeneraleSeance5(){
		$v = new VueGeneraleSeance5();
		$v->affiche();
	}

}
