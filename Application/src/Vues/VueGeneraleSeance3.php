<?php

namespace bd\Vues;
use bd\models\Character;
use bd\models\Company;
use bd\models\Enemies;
use bd\models\Friends;
use bd\models\Game;
use bd\models\Platform;
use bd\models\Rating_board;

use Illuminate\Support\Facades\DB;

class VueGeneraleSeance3{

  public function affiche(){
    echo "<br>";
  $jeu = Game::where('name', 'LIKE', '%Mario%')
    ->get();
  echo "Affichage des jeu comportant Mario dans leur nom";
  echo "<br>";
  foreach ($jeu as $jeuM) {
    echo $jeuM-> name, '<br>';
  }
  echo "<br>";

  $jeuMario = Game::where('name', 'LIKE', 'Mario%')
    ->get();
  echo "Affichage des jeux dont le nom commence par Mario :" . "<br/>";
  foreach ($jeuMario as $nom) {
    echo $nom-> name, '<br>';
  }

  $jeuMario3 = Game::where()

  //Test de la requête avec 3 valeurs différentes
  $timestamp_debut = microtime_float(true);
  $jeu = Game::where('name', 'LIKE', 'Mario%')
    ->get();
  echo "Affichage des jeux dont le nom commence par Mario :" . "<br/>";
  foreach ($jeuMario as $nom) {
    echo $nom-> name, '<br>';
  }
  $timestamp_fin = microtime_float(true);
  $difference = $timestamp_fin - $timestamp_debut;
  echo $difference;

  $timestamp_debut = microtime_float(true);
  $jeu = Game::where('name', 'LIKE', 'Zelda%')
    ->get();
  echo "Affichage des jeux dont le nom commence par Mario :" . "<br/>";
  foreach ($jeuMario as $nom) {
    echo $nom-> name, '<br>';
  }
  $timestamp_fin = microtime_float(true);
  $difference = $timestamp_fin - $timestamp_debut;
  echo $difference;

  $timestamp_debut = microtime_float(true);
  $jeu = Game::where('name', 'LIKE', 'The%')
    ->get();
  echo "Affichage des jeux dont le nom commence par Mario :" . "<br/>";
  foreach ($jeuMario as $nom) {
    echo $nom-> name, '<br>';
  }
  $timestamp_fin = microtime_float(true);
  $difference = $timestamp_fin - $timestamp_debut;
  echo $difference;

  /*Creation d'un index sur la colonne name de la table game
    CREATE INDEX ind_name
    ON game(name);
  */

  //Test de la requête avec 3 valeurs différentes après avoir créé l'index
  $timestamp_debut = microtime_float(true);
  $jeu = Game::where('name', 'LIKE', 'Mario%')
    ->get();
  echo "Affichage des jeux dont le nom commence par Mario :" . "<br/>";
  foreach ($jeuMario as $nom) {
    echo $nom-> name, '<br>';
  }
  $timestamp_fin = microtime_float(true);
  $difference = $timestamp_fin - $timestamp_debut;
  echo $difference;

  $timestamp_debut = microtime_float(true);
  $jeu = Game::where('name', 'LIKE', 'Zelda%')
    ->get();
  echo "Affichage des jeux dont le nom commence par Mario :" . "<br/>";
  foreach ($jeuMario as $nom) {
    echo $nom-> name, '<br>';
  }
  $timestamp_fin = microtime_float(true);
  $difference = $timestamp_fin - $timestamp_debut;
  echo $difference;

  $timestamp_debut = microtime_float(true);
  $jeu = Game::where('name', 'LIKE', 'The%')
    ->get();
  echo "Affichage des jeux dont le nom commence par Mario :" . "<br/>";
  foreach ($jeuMario as $nom) {
    echo $nom-> name, '<br>';
  }
  $timestamp_fin = microtime_float(true);
  $difference = $timestamp_fin - $timestamp_debut;
  echo $difference;

  //On active le log de requêtes SQL
  //Lister les noms de jeux dont le nom contient Mario
  $jeuMario = Game::where('name', 'LIKE', '%Mario%')
    ->toSql();
  echo "Affichage des jeux dont le nom contient Mario :" . "<br/>";
  foreach ($jeuMario as $nom) {
    echo $nom-> name, '<br>';
  }
}
