<?php

namespace bd\Vues;
use bd\models\Character;
use bd\models\Company;
use bd\models\Enemies;
use bd\models\Friends;
use bd\models\Game;
use bd\models\Platform;
use bd\models\Rating_board;
use bd\models\Utilisateur;

use Illuminate\Support\Facades\DB;


class VueGeneraleSeance5{
	
//Partie 1
   public static function accederAUnJeu($id,$app)
    {
        try{
            $g=m\Game::select('id','name','alias','deck','description','original_release_date','created_at')
                ->where("id","=",$id)->firstOrFail();
        }catch (ModelNotFoundException $e){
            $app->response->setStatus(404);
            echo json_encode(["msg"=>"game $id not found"]);
            return null;
        }
        return json_encode($g,JSON_FORCE_OBJECT | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

//Partie 2 
  public static function accederAUneCollectionDeJeux($jeu)
  {
    $app=\Slim\Slim::getInstance();
    if($jeu!=null){
      $name=$jeu->name;
      $alias=$jeu->alias;
      $deck=$jeu->deck;
      $description=$jeu->description;
      $original_release_date=$jeu->original_release_date;
      $url=$app->urlFor("games",array("id"=>$jeu->id));
	  $comments=$app->urlFor("commentaires",array("id"=>$jeu->id));
	  $characters=$app->urlFor("characters",array("id"=>$jeu->id));
      $game=[
        "id"=>$jeu->id,
        "name"=>$name,
        "alias"=>$alias,
        "deck"=>$deck,
        "description"=>$description,
        "original_release_date"=>$original_release_date,
      ];
    } else {
      $game=[
        "msg"=>"Game $id not found",
      ];
      $url="";
    }
    $array=[
      "game" => $game,
      "link"=> [
        "self"=>[
          "href"=>$url,
          ],
		 "comments"=>[
			"href"=>$comments,
          ],
		  "characters"=>[
			"href"=>$characters,
          ],
      ],
    ];
    return $array;
  }

//Partie 3
  public static function pagination($page){
    $app=\Slim\Slim::getInstance();
    $jeux=m\Game::listerLes200PremiersJeux($page*200);
    $games=array();
    foreach ($jeux as $j) {
      array_push($games, VueGeneraleSeance5::accederAUneCollectionDeJeux($j));
    }
    $url=$app->urlFor("allgames")."?page=";
    if($page==0){
      $prev=$page;
    }else {
      $prev=$page-1;
    }
    $next=$page+1;
    $array=array("games"=>$games,
                "links"=>[
                  "prev"=>[
                    "href"=>$url.$prev,
                    ],
                  "next"=>[
                    "href"=>$url.$next,
                    ],

                  ]);
    $json=json_encode($array, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    return $json;
  }


   

//Partie 5
    public static function commentaireSurLesjeux($id)
    {

      $app=\Slim\Slim::getInstance();
      $commentaire=Commentaire::commentairesJeu($id);
      $array=array();
      foreach ($commentaire as $c) {
          $user=Utilisateur::find($c->id_utilisateur);
          $com=[
            "id"=>$c->id,
            "titre"=>$c->titre,
            "contenu"=>$c->contenu,
            "dateCreation"=>$c->dateCreation,
            "user"=>[
                "name"=>$user->nom,
                "prenom"=>$user->prenom,
                "id"=>$user->id
            ]
          ];
          array_push($array,$com);
    }
    return json_encode($array,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
  }

  
	

}
