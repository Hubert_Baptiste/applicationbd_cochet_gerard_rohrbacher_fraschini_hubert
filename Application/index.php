<?php

require 'vendor/autoload.php';
use bd\Controleur\Controleur;
$app = new \Slim\Slim();
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$db->addConnection( parse_ini_file("src/config/conf.ini") );
$db->setAsGlobal();
$db->bootEloquent();

$app->get('/', function () {
    echo "Hello, ";
});

$app->get('/seance1', function () {
	 echo "test";
	
	$c = new Controleur();
	$c->afficherVueGeneraleSeance1();
});

$app->get('/seance2', function () {
	 echo "test";
	
	$c = new Controleur();
	$c->afficherVueGeneraleSeance2();
});

$app->get('/seance4', function () {
	 echo "test";
	
	$c = new Controleur();
	$c->afficherVueGeneraleSeance4();
});

$app->run();